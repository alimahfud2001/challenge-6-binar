package com.example.challenge6.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.challenge6.R
import com.example.challenge6.databinding.MovieItemBinding
import com.example.challenge6.fragments.HomeFragmentDirections
import com.example.challenge6.model.GetMovieResponseItem

class MovieAdapter(private val context: Context): RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<GetMovieResponseItem>(){
        override fun areItemsTheSame(
            oldItem: GetMovieResponseItem,
            newItem: GetMovieResponseItem
        ): Boolean = oldItem.id == newItem.id
        override fun areContentsTheSame(
            oldItem: GetMovieResponseItem,
            newItem: GetMovieResponseItem
        ): Boolean = oldItem.hashCode() == newItem.hashCode()
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    fun submitData(value: List<GetMovieResponseItem>?) = differ.submitList(value)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieAdapter.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(MovieItemBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: MovieAdapter.ViewHolder, position: Int) {
        val data = differ.currentList[position]
        data.let { holder.bind(data) }
        holder.itemView.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(data)
            it.findNavController().navigate(action)
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    inner class ViewHolder(private val binding: MovieItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(data: GetMovieResponseItem){
            binding.apply {
                Glide.with(context)
                    .load("https://image.tmdb.org/t/p/w342${data.posterPath}")
                    .placeholder(R.drawable.ic_smile)
                    .error(R.drawable.ic_error)
                    .centerCrop()
                    .into(ivRv)
                tvRvTitle.setText(data.title)
                tvRvRelease.setText(data.releaseDate)
            }
        }
    }
}