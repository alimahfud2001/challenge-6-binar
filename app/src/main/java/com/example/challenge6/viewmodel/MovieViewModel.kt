package com.example.challenge6.viewmodel

import android.util.Log
import androidx.lifecycle.*
import com.example.challenge6.manager.DataStoreManager
import com.example.challenge6.model.GetMovieResponse
import com.example.challenge6.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.example.challenge6.model.GetMovieResponseItem
import kotlinx.coroutines.launch

class MovieViewModel(private val pref: DataStoreManager): ViewModel() {
    var movieList = MutableLiveData<List<GetMovieResponseItem>>()
    var code: Int? = null

    fun fetchAllData() {
        ApiClient.instance.getTopRated()
            .enqueue(object : Callback<GetMovieResponse> {
                override fun onResponse(
                    call: Call<GetMovieResponse>,
                    response: Response<GetMovieResponse>
                ) {
                    movieList.postValue(response.body()?.results)
                    code = response.code()
                }

                override fun onFailure(call: Call<GetMovieResponse>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }
            })
    }

    fun saveDataStore(email:String, data: String) {
        viewModelScope.launch {
            pref.setData(email, data)
        }
    }

    fun saveLoginStatus(status:String) {
        viewModelScope.launch {
            pref.setLoginStatus(status)
        }
    }

    fun getDataStore(email: String) : LiveData<String> {
        return pref.getData(email).asLiveData()
    }

    fun getLoginStatus() : LiveData<String> {
        return pref.getLoginStatus().asLiveData()
    }

}