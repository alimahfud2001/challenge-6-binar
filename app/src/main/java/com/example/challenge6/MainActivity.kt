package com.example.challenge6

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment

class MainActivity : AppCompatActivity() {
    companion object {
        private const val IS_LOGGED_IN = "isLoggedIn"
        fun open(context: Context, isLoggedIn: String?) {
            context.startActivity(Intent(context, MainActivity::class.java).apply {
                putExtra(IS_LOGGED_IN, isLoggedIn)
            })
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        val navGraph = navController.navInflater.inflate(R.navigation.nav_graph)

        if (intent.getStringExtra(IS_LOGGED_IN).isNullOrEmpty())
            navGraph.setStartDestination(R.id.loginFragment)
        else navGraph.setStartDestination(R.id.homeFragment)

        navController.graph = navGraph
    }
}