package com.example.challenge6.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.challenge6.databinding.FragmentRegisterBinding
import com.example.challenge6.manager.DataStoreManager
import com.example.challenge6.viewmodel.MovieViewModel
import com.example.challenge6.viewmodel.ViewModelFactory
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class RegisterFragment : Fragment() {
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewmodel: MovieViewModel
    private lateinit var pref: DataStoreManager
    private var data = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pref = DataStoreManager(requireContext())
        viewmodel = ViewModelProvider(requireActivity(), ViewModelFactory(pref))[MovieViewModel::class.java]

        binding.et2.setOnFocusChangeListener({ v, hasFocus -> if (!hasFocus) observer(binding.et2.text.toString()) })

        binding.btnDaftar.setOnClickListener {
            val username = binding.ti1.editText?.text.toString()
            val email = binding.ti2.editText?.text.toString()
            val password = binding.ti3.editText?.text.toString()
            val confirmPassword = binding.ti4.editText?.text.toString()
            val gson = Gson()

            if (username.isNullOrEmpty()||email.isNullOrEmpty()||password.isNullOrEmpty()||confirmPassword.isNullOrEmpty())
                Toast.makeText(context, "Isi Detail", Toast.LENGTH_SHORT).show()
            else {
                val set = gson.fromJson<ArrayList<String>>(data, object : TypeToken<ArrayList<String>>(){}.type)
                if (set?.elementAt(1) != null) {
                    val dialog = AlertDialog.Builder(requireContext())
                    dialog.setTitle("Pendaftaran Gagal")
                    dialog.setMessage("Email sudah terdaftar")
                    dialog.setPositiveButton("Ok"){ dialogInterface, p1 -> }
                    dialog.show()
                }
                else {
                    if (password != confirmPassword) {
                        Toast.makeText(context, "Password tidak cocok", Toast.LENGTH_SHORT).show()
                    }
                    else {
                        val _set = arrayListOf(username, email, password)
                        viewmodel.saveDataStore(email, gson.toJson(_set))
                        val dialog = AlertDialog.Builder(requireContext())
                        dialog.setTitle("Pendaftaran Berhasil")
                        dialog.setMessage("Silahkan Login")
                        dialog.setPositiveButton("Login"){ dialogInterface, p1 ->
                            view.findNavController().popBackStack()
                        }
                        dialog.show()
                    }
                }
            }
        }
    }

    private fun observer(email:String) {
        viewmodel.apply {
            getDataStore(email).observe(requireActivity()) {
                data = it
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}