package com.example.challenge6.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.challenge6.databinding.FragmentDetailBinding
import com.bumptech.glide.Glide
import com.example.challenge6.R

class DetailFragment : Fragment() {
    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DetailFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: android.view.View, savedInstanceState: android.os.Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(requireContext())
            .load("https://image.tmdb.org/t/p/w342${args.currentItem.posterPath}")
            .placeholder(R.drawable.ic_smile)
            .error(R.drawable.ic_error)
            .centerCrop()
            .into(binding.ivDetail)
        binding.tvDetailTitle.setText(args.currentItem.title)
        binding.tvDetailRating.setText("${args.currentItem.voteAverage.toString()}")
        binding.tvDetailOverview.setText(args.currentItem.overview)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}