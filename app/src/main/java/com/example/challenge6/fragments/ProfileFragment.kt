package com.example.challenge6.fragments

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.contract.ActivityResultContracts.GetContent
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.challenge6.R
import com.example.challenge6.SplashActivity
import com.example.challenge6.databinding.FragmentProfileBinding
import com.example.challenge6.manager.DataStoreManager
import com.example.challenge6.viewmodel.MovieViewModel
import com.example.challenge6.viewmodel.ViewModelFactory
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_logout_dialog.view.*
import java.io.ByteArrayOutputStream


class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding
    private lateinit var viewmodel: MovieViewModel
    private lateinit var pref: DataStoreManager
    private var email = ""
    private var data = ""
    val gson = Gson()

   override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       _binding = FragmentProfileBinding.inflate(inflater, container, false)
       return binding?.root
   }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mGetContent = registerForActivityResult(GetContent(), ActivityResultCallback<Uri> { uri -> binding?.ivProfile?.setImageURI(uri)})
        pref = DataStoreManager(requireContext())
        viewmodel = ViewModelProvider(requireActivity(), ViewModelFactory(pref))[MovieViewModel::class.java]

        observer()

        binding?.btnSave?.setOnClickListener {
            val set = gson.fromJson<ArrayList<String>>(data, object : TypeToken<ArrayList<String>>(){}.type)
            if (!binding?.et1?.text.isNullOrEmpty()|| !binding?.et2?.text.isNullOrEmpty()||
                !binding?.et3?.text.isNullOrEmpty()|| !binding?.et4?.text.isNullOrEmpty()){
                val baos = ByteArrayOutputStream()
                binding?.ivProfile?.drawable?.toBitmap()?.compress(Bitmap.CompressFormat.PNG, 100, baos)
                val b: ByteArray = baos.toByteArray()
                val encoded: String = Base64.encodeToString(b, Base64.DEFAULT)

                val _set = arrayListOf(
                    binding?.et1?.text.toString(),
                    binding?.et2?.text.toString(),
                    set.elementAt(2),
                    binding?.et3?.text.toString(),
                    binding?.et4?.text.toString(),
                    encoded)
                viewmodel.saveDataStore(binding?.et2?.text.toString(), gson.toJson(_set))
                Toast.makeText(requireContext(), "Data Berhasil Disimpan", Toast.LENGTH_SHORT).show()
            }
            else Toast.makeText(requireContext(), "Isi Semua Data", Toast.LENGTH_SHORT).show()
        }

        binding?.ivProfile?.setOnClickListener {
            mGetContent.launch("image/*")
        }


        binding?.btnLogout?.setOnClickListener {
            val view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_logout_dialog, null, false)
            val dialogBuilder = AlertDialog.Builder(requireContext())
            dialogBuilder.setView(view)
            val dialog = dialogBuilder.create()
            dialog.show()
            view.btndeleteyes.setText("Logout")
            view.tvdelete.setText("Logout Akun?")

            view.btndeleteyes.setOnClickListener {
                viewmodel.saveLoginStatus("null")
                startActivity(Intent(activity, SplashActivity::class.java))
                requireActivity().finishAffinity()
                requireActivity().finish()
                dialog?.dismiss()
            }
            view.btndeleteno.setOnClickListener {
                dialog?.dismiss()
            }
        }
    }

    private fun loadData(set: ArrayList<String>) {
        binding?.et1?.setText(set.elementAt(0))
        binding?.et2?.setText(set.elementAt(1))

        if (set.size >= 6){
            binding?.et3?.setText(set.elementAt(3))
            binding?.et4?.setText(set.elementAt(4))
            val imageAsBytes: ByteArray = Base64.decode(set.elementAt(5).toByteArray(), Base64.DEFAULT)
            binding?.ivProfile?.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.size))
        }
    }

    private fun observer() {
        viewmodel.apply {
            getLoginStatus().observe(requireActivity()) {
                email = it
                if (isAdded){
                    getDataStore(email).observe(requireActivity()){
                        data = it
                        val set = gson.fromJson<ArrayList<String>>(data, object : TypeToken<ArrayList<String>>(){}.type)
                        if (set!=null) loadData(set)
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}