package com.example.challenge6.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.challenge6.R
import com.example.challenge6.databinding.FragmentLoginBinding
import com.example.challenge6.manager.DataStoreManager
import com.example.challenge6.viewmodel.MovieViewModel
import com.example.challenge6.viewmodel.ViewModelFactory
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewmodel: MovieViewModel
    private lateinit var pref: DataStoreManager
    private var dataJson = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val gson = Gson()

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                activity?.finish()
            }
        })

        binding.ti1.editText?.setOnFocusChangeListener({ v, hasFocus -> if (!hasFocus) observer(binding.ti1.editText!!.text.toString()) })

        pref = DataStoreManager(requireContext())
        viewmodel = ViewModelProvider(requireActivity(), ViewModelFactory(pref))[MovieViewModel::class.java]

        binding.btnLogin.setOnClickListener {
            val email = binding.ti1.editText?.text.toString()
            val password = binding.ti2.editText?.text.toString()
            val set = gson.fromJson<ArrayList<String>>(dataJson, object : TypeToken<ArrayList<String>>(){}.type)
            if (email.isEmpty()||password.isEmpty())
                Toast.makeText(requireContext(), "Isi Detail", Toast.LENGTH_SHORT).show()
            else {
                if (set?.elementAt(1) == null)
                    Toast.makeText(requireContext(), "Email Tidak Terdaftar", Toast.LENGTH_SHORT).show()
                else{
                    if (password != set.elementAt(2))
                        Toast.makeText(requireContext(), "Email/Password Salah", Toast.LENGTH_SHORT).show()
                    else {
                        viewmodel.saveLoginStatus(email)
                        it.findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
                    }
                }
            }
        }

        binding.tvblmpnya.setOnClickListener{
            it.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }

    private fun observer(email:String) {
        viewmodel.apply {
            getDataStore(email).observe(requireActivity()) {
                dataJson = it
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}