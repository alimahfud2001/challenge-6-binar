package com.example.challenge6.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.challenge6.R
import com.example.challenge6.adapter.MovieAdapter
import com.example.challenge6.databinding.FragmentHomeBinding
import com.example.challenge6.manager.DataStoreManager
import com.example.challenge6.model.GetMovieResponseItem
import com.example.challenge6.viewmodel.MovieViewModel
import com.example.challenge6.viewmodel.ViewModelFactory
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding
    private lateinit var viewmodel: MovieViewModel
    private lateinit var pref: DataStoreManager
    private var email = ""
    private var data = ""
    private val gson = Gson()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding?.root
         }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pref = DataStoreManager(requireContext())
        viewmodel = ViewModelProvider(requireActivity(), ViewModelFactory(pref))[MovieViewModel::class.java]
        observer()

        viewmodel.movieList.observe(requireActivity(), Observer {
            Log.d("listMovie", "$it")
            if (viewmodel.code == 200){
                showList(it)
                binding?.progressBar?.visibility = View.GONE
            }
            else binding?.progressBar?.visibility = View.GONE
        })
        viewmodel.fetchAllData()

        binding?.ivHomeToprofil?.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_profileFragment)
        }
    }

    private fun observer() {
        viewmodel.apply {
            getLoginStatus().observe(requireActivity()){
                email = it
                viewmodel.getDataStore(email).observe(requireActivity()){
                    data = it
                    val set = gson.fromJson<ArrayList<String>>(data, object : TypeToken<ArrayList<String>>(){}.type)
                    if (set!=null) binding?.tvHomeHello?.setText("Hi, ${set.elementAt(0)} ")
                }
            }

        }
    }

    private fun showList(data: List<GetMovieResponseItem>?){
        val adapter = MovieAdapter(requireContext())
        adapter.submitData(data)
        binding?.recyclerview?.setLayoutManager(GridLayoutManager(requireContext(), 3))
        binding?.recyclerview?.adapter = adapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}