package com.example.challenge6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import androidx.lifecycle.ViewModelProvider
import com.example.challenge6.databinding.ActivitySplashBinding
import com.example.challenge6.manager.DataStoreManager
import com.example.challenge6.viewmodel.MovieViewModel
import com.example.challenge6.viewmodel.ViewModelFactory

class SplashActivity : AppCompatActivity() {
    private lateinit var viewmodel: MovieViewModel
    private lateinit var pref: DataStoreManager
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        pref = DataStoreManager(this)
        viewmodel = ViewModelProvider(this, ViewModelFactory(pref))[MovieViewModel::class.java]

        var isLoggedIn = ""
        viewmodel.getLoginStatus().observe(this){
            isLoggedIn = it
        }

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        Handler().postDelayed({
            when (isLoggedIn){
                "null" -> MainActivity.open(this, null)
                else -> MainActivity.open(this, isLoggedIn)
            }
            finish()
        }, 2000)
    }
}